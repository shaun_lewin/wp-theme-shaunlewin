<?php get_header(); ?>

	<main role="main">
		<section id="profile" class="container-fluid">
		    <div class="row ig-sierra">
		        <img id="profile-pic" alt="my-pic" src="<?php echo get_template_directory_uri(); ?>/assets/img/profile.jpg"/>
		    </div>
		    <section class="intro text-center">
		        <h2>Shaun Lewin</h2>
		        <h5>Web Developer</h2>
		        <p>
		          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
		        </p>
		    </section>
    </section>
		<section id="resume" class="container-fluid">
        <h2 class="text-center">Resume</h2>
        <p class="objective hidden">
            To obtain a position that will utilize my skills in web development.
        </p>
        <div class="education text-center">
            <span class="fa fa-graduation-cap fa-2x"></span>
            <h4 class="text-center">Education</h4>
            <b>Associates of Science Degree</b><br/>
            <b>Valencia College</b> - <i>Orlando, FL</i><br/>
            <b>Major:</b> <i>Computer Programming & Analysis</i><br/>
            <b>Graduation Date:</b> <i>December 2010</i>
            <div class="seperator"></div>
        </div>
        <div class="work-history text-center">
            <span class="fa fa-history fa-2x"></span>
            <h4 class="text-center">Work History</h4>
            <div class="job">
                <div class="button bookit-bg">
                    <span class="company-name">BookIt.com</span>
                    <span class="glyphicon glyphicon-menu-down arrow"></span>
                </div>
                <div class="details light-green container-fluid">
                    <div class="timeline">
                        <div class="clearfix row">
                            <span class="pull-left text-left date col-xs-6">May 2014</span>
                            <span class="pull-right text-right date col-xs-6">November 2015</span>
                        </div>
                        <div class="row">
                            <div class="line">
                                <span class="pull-left point light-green"></span>
                                <span class="pull-right point light-green"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <span><i class="fa fa-map-marker fa-lg"></i> Maitland, FL</span>
                    </div>
                    <ul class="duties">
                        <li>Developed customer lead management system, using PHP and jQuery UI, which was used by the sales team to track visitor's actions on the company site</li>
                        <li>Identified and resolved PHP and Javascript bugs</li>
                        <li>Complete development of a blog site containing key words to lead ot main site for sales.  Involed customization of a WordPress theme.</li>
                        <li>Development of user tracking program to identify key referral web sources.</li>
                        <li>Converted Photoshop designs to HTML/CSS</li>
                    </ul>
                </div>
            </div>
            <div class="job">
                <div class="button ffa-bg">
                    <span class="company-name">Fast Forward Academy</span>
                    <span class="glyphicon glyphicon-menu-down arrow"></span>
                </div>
                <div class="details light-blue-100 container-fluid">
                    <div class="timeline">
                        <div class="clearfix row">
                            <span class="pull-left text-left date col-xs-6">October 2012</span>
                            <span class="pull-right text-right date col-xs-6">March 2014</span>
                        </div>
                        <div class="row">
                            <div class="line">
                              <span class="pull-left light-blue-100 point"></span>
                              <span class="pull-right light-blue-100 point"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <span><i class="fa fa-map-marker fa-lg"></i> Altamonte Springs, FL</span>
                    </div>
                    <ul class="duties">
                        <li>Developed customer lead management system, using PHP and jQuery UI, which was used by the sales team to track visitors' actions on the company site</li>
                        <li>Identified and resolved PHP and Javascript bugs</li>
                        <li>Complte development of a blog site containing key words to lead ot main site for sales.  INvoled customization of a WordPress theme.</li>
                        <li>Development of user tracking program to identify key referral web sources.</li>
                        <li>Converted Photoshop designs to HTML/CSS</li>
                    </ul>
                </div>
            </div>
            <div class="job">
                <div class="button csky-bg">
                    <span class="company-name">Clearsky Technologies</span>
                    <span class="glyphicon glyphicon-menu-down arrow"></span>
                </div>
                <div class="details light-blue-100 container-fluid">
                    <div class="timeline">
                        <div class="clearfix row">
                            <span class="pull-left text-left date col-xs-6">June 2010</span>
                            <span class="pull-right text-right date col-xs-6">September 2012</span>
                        </div>
                        <div class="row">
                            <div class="line">
                              <span class="pull-left light-blue-100 point"></span>
                              <span class="pull-right light-blue-100 point"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <span><i class="fa fa-map-marker fa-lg"></i> Orlando, FL</span>
                    </div>
                    <ul class="duties">
                        <li>Developed and implemented multiple processes that increased effiency of handset configuration for use with a hosted mobile content store</li>
                        <li>Identified and resolved PHP and Javascript bugs</li>
                        <li>Complte development of a blog site containing key words to lead ot main site for sales.  INvoled customization of a WordPress theme.</li>
                        <li>Development of user tracking program to identify key referral web sources.</li>
                        <li>Converted Photoshop designs to HTML/CSS</li>
                    </ul>
                </div>
            </div>
            <div class="job">
                <div class="button ups-bg">
                    <span class="company-name">United Parcel Service</span>
                    <span class="glyphicon glyphicon-menu-down arrow"></span>
                </div>
                <div class="details light-brown-100 container-fluid">
                    <div class="timeline">
                        <div class="clearfix row">
                            <span class="pull-left text-left date col-xs-6">August 2004</span>
                            <span class="pull-right text-right date col-xs-6">December 2010</span>
                        </div>
                        <div class="row">
                            <div class="line">
                              <span class="pull-left light-brown-100 point"></span>
                              <span class="pull-right light-brown-100 point"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <span><i class="fa fa-map-marker fa-lg"></i> Orlando, FL</span>
                    </div>
                    <ul class="duties">
                        <li>Sorted an average of 1300 packages per hour by zip code to proper destinations with less than 5% error rate</li>
                        <li>Distributed irregular shaped packages and overweight packages to trailers for delivery</li>
                        <li>Participated with safety committee to ensure the compliance of safety procedures amongst other employees</li>
                        <li>Achieved 5 years of safety compliance with no work-related injuries</li>
                    </ul>
                </div>
            </div>
            <div class="seperator"></div>
        </div>
        <div class="skills text-center">
            <span class="fa fa-code fa-2x"></span>
            <h4 class="text-center">Skills</h4>
            <div id="owl-carousel">
                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/html5.png"/></div>
                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/css3.png"/></div>
                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/javascript.png"/></div>
                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/php.png"/></div>
                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/mysql.png"/></div>
                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/git.png"/></div>
            </div>
        </div>
    </section>
		<section id="portfolio" class="container-fluid">
          <h2 class="text-center">Portfolio</h2>
          <div class="links">
              <a class="btn btn-default btn-block">
                  <i class="fa fa-globe fa-3x"></i>
                  <p>Sites</p>
              </a>
              <a class="btn btn-default btn-block">
                  <i class="fa fa-github fa-3x"></i>
                  <p>Github</p>
              </a>
              <a class="btn btn-default btn-block">
                  <i class="fa fa-bitbucket fa-3x"></i>
                  <p>Bitbucket</p>
		          </a>
              <a class="btn btn-default btn-block">
                  <i class="fa fa-codepen fa-3x"></i>
                  <p>Codepen.io</p>
              </a>
          </div>
      </section>
      <section id="contact" class="container-fluid">
          <h3 class="text-center">Contact</h3>
          <form class="form-horizontal">
							<input type="hidden" name="csrf_token" value=""/>
							<div class="form-group">
									<label for="name" class="hidden-xs col-sm-2 control-label">Name</label>
									<div class="col-xs-12 col-sm-8">
              				<input type="text" class="form-control input-lg" id="name" name="name" placeholder="Name" required/>
									</div>
							</div>
							<div class="form-group">
									<label for="email" class="hidden-xs col-sm-2 control-label">Email</label>
									<div class="col-xs-12 col-sm-8">
              				<input type="email" class="form-control input-lg" id="email" name="email" placeholder="Email" required/>
									</div>
							</div>
							<div class="form-group">
									<label for="company" class="hidden-xs col-sm-2 control-label">Company</label>
									<div class="col-xs-12 col-sm-8">
              				<input type="text" class="form-control input-lg" id="company" name="company" placeholder="Company"/>
									</div>
							</div>
							<div class="form-group">
									<label for="company" class="hidden-xs col-sm-2 control-label">Message</label>
									<div class="col-xs-12 col-sm-8">
              				<textarea class="form-control input-lg" name="message" cols="20" rows="7" placeholder="Message" required></textarea>
									</div>
							</div>
							<div class="form-group">
									<div class="col-xs-12 col-sm-8 col-sm-offset-2">
											<input type="submit" class="btn btn-primary form-control input-lg" value="Submit">
									</div>
							</div>
          </form>
      </section>
	</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
