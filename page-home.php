<?php get_header(); ?>

	<main role="main">
            <section class="intro container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-lg-6 col-centered">
                        <article id="intro" class="slide-down">
                            <?php
                                if(have_posts()) {
                                    while(have_posts()) {
                                        the_post();
                                        the_content();
                                   }
                                }
                            ?>
                        </article>                    
                    </div>                
                </div>
            </section>
	</main>

<?php get_footer(); ?>
