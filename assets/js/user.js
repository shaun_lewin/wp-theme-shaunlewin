(function($, window, document){

    $(document).ready(function(){

        //Start Carousel
        $('#owl-carousel').owlCarousel({
            items: 3,
            itemsTablet: [768,3],
            responsive: true,
            responsiveRefreshRate: 500,
            slideSpeed: 400,            
        });

        //Handle work history buttons
        $('.work-history').on('click', '.job', function() {
              var job = $(this);
              job.find('.details').toggleClass('show');
        });

        //Navigation
        $('.nav').on('click', 'li', function() {
            var text = this.innerText;
            if(text.length) {
                var section = $('#' + text.toLowerCase());
                section.velocity("scroll", {});
            }
        });
        
        $('#hamburger-icon').on('click', function(){
              $('body').toggleClass('nav-open');
        });        
    });



})(jQuery, window, document);
