<?php get_header(); ?>

	<main role="main">
		<section id="resume" class="container-fluid">
                    <h2 class="sub-title text-center">Resume</h2>                   
                    <div class="education text-center">
                        <span class="fa fa-graduation-cap fa-2x"></span>
                        <h3 class="section-title text-center">Education</h3>
                        <b>Associates of Science Degree</b><br/>
                        <b>Valencia College</b> - <i>Orlando, FL</i><br/>
                        <b>Major:</b> <i>Computer Programming & Analysis</i><br/>
                        <b>Graduation Date:</b> <i>December 2010</i>
                        <div class="seperator"></div>
                    </div>
                    <div class="row">
                        <div class="work-history col-md-6 col-md-offset-3">
                            <div class="text-center">
                                <span class="fa fa-history fa-2x"></span>
                                <h3 class="section-title text-center">Work History</h3>
                            </div>
                            <div class="job">
                                <div class="button bookit-bg">
                                    <span class="company-name">BookIt.com</span>
                                    <span class="glyphicon glyphicon-menu-down arrow"></span>
                                </div>
                                <div class="details light-green container-fluid">
                                    <div class="timeline">
                                        <div class="clearfix row">
                                            <span class="pull-left text-left date col-xs-6">May 2014</span>
                                            <span class="pull-right text-right date col-xs-6"></span>
                                        </div>
                                        <div class="row">
                                            <div class="line">
                                                <span class="pull-left point light-green"></span>
                                                <span class="pull-right point light-green"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row text-center">
                                        <span><i class="fa fa-map-marker fa-lg"></i> Maitland, FL</span>
                                    </div>
                                    <ul class="duties">
                                        <li>Developed in an agile environment</li>
                                        <li>Worked with Google Analytics and other 3rd party web analytics software on company site</li>
                                        <li>Executed QA test plans as needed</li>
                                        <li>Used Bootstrap and Mustache.js to implement Photoshop designs for responsive redesign of the company site</li>
                                        <li>Debugged a wide variety of front end issues</li>
                                        <li>Focused on writing unit testable code for PHPUnit</li>
                                        <li>Adhered to HTML, CSS, Javascript, MySQL, and PHP  coding standards</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="job">
                                <div class="button ffa-bg">
                                    <span class="company-name">Fast Forward Academy</span>
                                    <span class="glyphicon glyphicon-menu-down arrow"></span>
                                </div>
                                <div class="details light-blue-100 container-fluid">
                                    <div class="timeline">
                                        <div class="clearfix row">
                                            <span class="pull-left text-left date col-xs-6">October 2012</span>
                                            <span class="pull-right text-right date col-xs-6">March 2014</span>
                                        </div>
                                        <div class="row">
                                            <div class="line">
                                                <span class="pull-left light-blue-100 point"></span>
                                                <span class="pull-right light-blue-100 point"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row text-center">
                                        <span><i class="fa fa-map-marker fa-lg"></i> Altamonte Springs, FL</span>
                                    </div>
                                    <ul class="duties">
                                        <li>Developed customer lead management system, using PHP and jQuery UI, which was used by the sales team to track visitors' actions on the company site</li>
                                        <li>Identified and resolved PHP and Javascript bugs</li>
                                        <li>Complete development of a blog containing key words to help convert sales</li>
                                        <li>Development of user tracking program to identify key referral web sources</li>
                                        <li>Converted Photoshop designs to HTML/CSS</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="job">
                                <div class="button csky-bg">
                                    <span class="company-name">Clearsky Technologies</span>
                                    <span class="glyphicon glyphicon-menu-down arrow"></span>
                                </div>
                                <div class="details light-blue-100 container-fluid">
                                    <div class="timeline">
                                        <div class="clearfix row">
                                            <span class="pull-left text-left date col-xs-6">June 2010</span>
                                            <span class="pull-right text-right date col-xs-6">September 2012</span>
                                        </div>
                                        <div class="row">
                                            <div class="line">
                                            <span class="pull-left light-blue-100 point"></span>
                                            <span class="pull-right light-blue-100 point"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row text-center">
                                        <span><i class="fa fa-map-marker fa-lg"></i> Orlando, FL</span>
                                    </div>
                                    <ul class="duties">
                                        <li>Developed and implemented multiple processes that increased efficiency of handset configuration for use with a hosted mobile content store</li>
                                        <li>Provided Tier 2 support for issues involving mobile content downloads, MMS sending/receiving, and Wireless Application Protocol (WAP) connectivity</li>
                                        <li>Reduced the average amount of customer support tickets by over 50% within 4 months of employment</li>
                                        <li>Supported senior system administrator with monitoring and maintenance of hosted web applications in a Linux environment</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="job">
                                <div class="button ups-bg">
                                    <span class="company-name">United Parcel Service</span>
                                    <span class="glyphicon glyphicon-menu-down arrow"></span>
                                </div>
                                <div class="details light-brown-100 container-fluid">
                                    <div class="timeline">
                                        <div class="clearfix row">
                                            <span class="pull-left text-left date col-xs-6">August 2004</span>
                                            <span class="pull-right text-right date col-xs-6">December 2010</span>
                                        </div>
                                        <div class="row">
                                            <div class="line">
                                                <span class="pull-left light-brown-100 point"></span>
                                                <span class="pull-right light-brown-100 point"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row text-center">
                                        <span><i class="fa fa-map-marker fa-lg"></i> Orlando, FL</span>
                                    </div>
                                    <ul class="duties">
                                        <li>Sorted an average of 1300 packages per hour by zip code to proper destinations with less than 5% error rate</li>
                                        <li>Distributed irregular shaped packages and overweight packages to trailers for delivery</li>
                                        <li>Participated with safety committee to ensure the compliance of safety procedures amongst other employees</li>
                                        <li>Achieved 5 years of safety compliance with no work-related injuries</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="seperator"></div>
                        </div>
                    </div>                    
                    <div class="skills row">
                        <div class="text-center">
                            <span class="fa fa-code fa-2x"></span>
                            <h3 class="section-title text-center">Skills</h3>
                        </div>
                        <div class="col-md-6 col-md-offset-3">
                            <div id="owl-carousel">
                                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/html5.png"/></div>
                                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/css3.png"/></div>
                                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/bootstrap.png"/></div>
                                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/sass.png"/></div>
                                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/javascript.png"/></div>
                                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/php.png"/></div>
                                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/mysql.png"/></div>
                                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/linux.png"/></div>
                                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/git.png"/></div>                          
                                <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/mustache.png"/></div>
                            </div>
                        </div>                        
                    </div>
		</section>
	</main>

<?php get_footer(); ?>
