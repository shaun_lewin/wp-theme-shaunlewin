			<!-- footer -->
                        <p class="copyright">
                            Theme By:  <?php bloginfo('name'); ?>. <?php _e('Powered by', 'html5blank'); ?>
                            <a href="//wordpress.org" title="WordPress">WordPress</a> &amp; <a href="//html5blank.com" title="HTML5 Blank">HTML5 Blank</a>.
                        </p>
			<footer class="footer" role="contentinfo">                            
                            <nav class="nav slide-up hidden-md hidden-lg">
                                <ul>
                                    <li>
                                        <a href="<?php echo get_site_url(); ?>">
                                            <i class="fa fa-home"></i>Home
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo get_site_url(); ?>/resume">
                                            <i class="fa fa-file-o"></i>Resume
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo get_site_url(); ?>/portfolio">
                                            <i class="fa fa-folder-open"></i>Portfolio
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo get_site_url(); ?>/contact">
                                            <i class="fa fa-envelope"></i>Contact
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo get_site_url(); ?>/about">
                                            <i class="fa fa-info-circle"></i>About
                                        </a>
                                    </li>
                                </ul>
                            </nav>
			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>
		<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
                <script src="<?php echo get_template_directory_uri(); ?>/assets/bootstrap/js/bootstrap.min.js"></script>
                <script src="<?php echo get_template_directory_uri(); ?>/assets/js/owl-carousel/owl.carousel.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/assets/js/velocity.min.js"></script>
                <script src="<?php echo get_template_directory_uri(); ?>/assets/js/user.js"></script>

	</body>
</html>
