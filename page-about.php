<?php get_header(); ?>

	<main role="main">
            <section id="profile" class="container-fluid">
                <article class="intro">
                    <?php
                        if(have_posts()) {
                             while(have_posts()) {
                                 the_post();
                                 the_content();
                             }
                        }
                    ?>
                </article>
            </section>
	</main>

<?php get_footer(); ?>
