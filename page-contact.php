<?php get_header(); ?>

	<main role="main" class="container-fluid">
            <section id="contact">
                <h3 class="sub-title text-center">Contact Me</h3>
                <div class="form-errors col-md-6 col-centered hidden">                    
                </div>
                <form id="simple-ajax-form" class="form-horizontal col-xs-12 col-md-6 col-centered" method="post">
                    <input type="hidden" name="csrf_token" value=""/>
                    <div class="form-group">
                        <label for="name" class="control-label hidden-xs col-md-1">Name</label>
                        <input type="text" class="form-control input-lg" id="name" name="name" placeholder="Name"/>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label hidden-xs col-md-1">Email</label>
                        <input type="text" class="form-control input-lg" id="email" name="email" placeholder="Email"/>
                    </div>
                    <div class="form-group">
                        <label for="company" class="control-label hidden-xs col-md-1">Company</label>
                        <input type="text" class="form-control input-lg" id="company" name="company" placeholder="Company"/>
                    </div>
                    <div class="form-group">
                        <label for="company" class="control-label hidden-xs col-md-1">Message</label>
                        <textarea class="form-control input-lg" name="message" cols="20" rows="7" placeholder="Message"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary form-control input-lg" name="submitted" value="Send Message">
                    </div>
                </form>
            </section>
	</main>

<?php get_footer(); ?>
