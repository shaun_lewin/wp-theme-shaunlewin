<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
            <meta charset="<?php bloginfo('charset'); ?>">
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="description" content="<?php bloginfo('description'); ?>">            
            <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
            <link href="//www.google-analytics.com" rel="dns-prefetch">
            <link href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png" rel="shortcut icon">
            <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/bootstrap/css/bootstrap.min.css">
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/font-awesome-4.4.0/css/font-awesome.min.css">
            <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,900,700,500' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/user.css">
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animations.css">
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/js/owl-carousel/owl.carousel.css">
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/js/owl-carousel/owl.theme.css">            

            <?php wp_head(); ?>
            <script>
            // conditionizr.com
            // configure environment tests
            conditionizr.config({
                assets: '<?php echo get_template_directory_uri(); ?>',
                tests: {}
            });
            </script>

	</head>
	<body <?php body_class(); ?>>                
            <nav id="nav" class="hidden-xs hidden-sm pull-left">                
                <ul class="fa-ul">
                    <li>
                        <a href="<?php echo get_site_url(); ?>">
                            <i class="fa-li fa fa-home fa-fw"></i>Home
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo get_site_url(); ?>/resume">
                            <i class="fa-li fa fa-file-o fa-fw"></i>Resume
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo get_site_url(); ?>/portfolio">
                            <i class="fa-li fa fa-folder-open fa-fw"></i>Portfolio
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo get_site_url(); ?>/contact">
                            <i class="fa-li fa fa-envelope fa-fw"></i>Contact
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo get_site_url(); ?>/about">
                            <i class="fa-li fa fa-info-circle fa-fw"></i>About
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- wrapper -->
            <div class="site">

            <!-- header -->
            <header id="home">
                <!--<i id="hamburger-icon" class="fa fa-bars fa-3x hidden-xs hidden-sm"></i>-->
                <a href="http://www.shaunlewin.com" class="title">{ Shaun : Lewin }</a>
            </header>
            <!-- /header -->
                
